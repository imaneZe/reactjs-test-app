import React from "react";
import {render} from "react-dom";
import App from "./components/App";

const divApp = document.getElementById("app");
render(<App />, divApp);