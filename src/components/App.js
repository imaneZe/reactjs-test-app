import React, {Component} from "react";

import {config} from "../config";

import Loader from "./Loader";
import Popup from "./Popup";

class App extends Component {
	constructor(){
		super();
	}

	render(){
		return (
			<div>
				<h2>Hello world!</h2>
				<p>Lorem ipsum dolor si amet.</p>
			</div>
		);
	}
};

export default App;