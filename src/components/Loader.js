import React from "react";
const Loader = () => {
	return <div className="label label-default">Loading...</div>;
};

export default Loader;